package com.example.hidroino;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hidro.hidroino.R;

public class Jogo_Dominando extends Fragment {
	   View rootview; 
	   @Nullable
	   @Override
	   public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		rootview = inflater.inflate(R.layout.jogodominando, container, false);
		   	   return rootview;
		   
	   }
}
