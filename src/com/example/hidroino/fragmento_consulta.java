package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.hidro.hidroino.R;

public class fragmento_consulta extends Activity{
       //cria��o de variaveis 
	TextView edtdata, textView2,textView3,textView5;
	TextView data1, data2, barra1, barra2,porcem1, porcem2;
	Button button5,button3,button2,button4,button1,button6;
	TextView TextView01,TextView02,TextView05,TextView06; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragmento_consulta);
                //chamando mask para colocar mascara no edittext data
		final EditText editText1 = (EditText) findViewById(R.id.editText1);
		editText1.addTextChangedListener(Mask.insert("##/##/####", editText1));	
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));
	    //identificadores para os botoes, edttext e textview
	    button6 = (Button)findViewById(R.id.button6);
	    button6.setVisibility(View.GONE);
	    button1 = (Button)findViewById(R.id.button1);
	    button1.setVisibility(View.GONE);
		button5 = (Button)findViewById(R.id.button5);
		button5.setVisibility(View.GONE);
		button4 = (Button)findViewById(R.id.button4);
		button3 = (Button)findViewById(R.id.button3);
		button2 = (Button)findViewById(R.id.button2);
		textView5 = (TextView)findViewById(R.id.textView5);
		textView5.setVisibility(View.GONE);
		TextView01 = (TextView)findViewById(R.id.TextView01);
		TextView01.setVisibility(View.GONE);
		TextView02 = (TextView)findViewById(R.id.TextView02);
		TextView02.setVisibility(View.GONE);
		TextView05 = (TextView)findViewById(R.id.TextView05);
		TextView05.setVisibility(View.GONE);
		TextView06 = (TextView)findViewById(R.id.TextView06);
		TextView06.setVisibility(View.GONE);
		edtdata = (EditText)findViewById(R.id.editText1);
		data1 =(TextView)findViewById(R.id.TextView09);
		data1.setVisibility(View.INVISIBLE);
		data2 =(TextView)findViewById(R.id.textView6);
		data2.setVisibility(View.INVISIBLE);
		barra1 =(TextView)findViewById(R.id.TextView07);
		barra1.setVisibility(View.INVISIBLE);
		barra2 =(TextView)findViewById(R.id.textView8);
		barra2.setVisibility(View.INVISIBLE);
		porcem1 =(TextView)findViewById(R.id.TextView03);
		porcem1.setVisibility(View.INVISIBLE);
		porcem2 =(TextView)findViewById(R.id.textView10);
		porcem2.setVisibility(View.INVISIBLE);
		textView2=(TextView)findViewById(R.id.textView2);
		textView2.setVisibility(View.INVISIBLE);
		textView3=(TextView)findViewById(R.id.textView3);
		textView3.setVisibility(View.INVISIBLE);
		
		
     }
	
	String datacompara;
	int vermetodo=0;
	int datacomparatamanho;
	//metodo para o botao sele��o de dia 
	public void dia(View v){
	datacompara =(edtdata.getText().toString());
	datacomparatamanho = datacompara.length();
	
	if(datacomparatamanho < 10){
		Toast.makeText(getApplicationContext(), "Data inv�lida!!", Toast.LENGTH_LONG).show();		
	}else{
        Banco_de_dados bd = new Banco_de_dados(this);
        bd.fragmento_consultadia(this);
        if(TextView01.getText().toString().equals("Consumo")){
        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
        TextView06.setVisibility(View.INVISIBLE);
        TextView01.setVisibility(View.INVISIBLE);
        }else{
            button4.setEnabled(false);
            button3.setEnabled(false);
            button2.setEnabled(false);
        button5.setVisibility(View.VISIBLE);
        TextView06.setVisibility(View.VISIBLE);
        TextView01.setVisibility(View.VISIBLE);
        textView2.setVisibility(View.VISIBLE);
        button6.setVisibility(View.VISIBLE);
        edtdata.setText("");
        vermetodo=vermetodo + 1;
        }
	}
	
	}
       //metodo para o botao sele��o de mes
	public void mes(View v){
		datacompara =(edtdata.getText().toString());
		datacomparatamanho = datacompara.length();
		
		if(datacomparatamanho < 10){
			Toast.makeText(getApplicationContext(), "Data inv�lida!", Toast.LENGTH_LONG).show();		
		}else{
	        Banco_de_dados bd = new Banco_de_dados(this);
	        bd.fragmento_consultames(this);
	        if(TextView01.getText().toString().equals("Consumo")){
	        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
	        TextView06.setVisibility(View.INVISIBLE);
	        TextView01.setVisibility(View.INVISIBLE);
	        }else{
	            button4.setEnabled(false);
	            button3.setEnabled(false);
	            button2.setEnabled(false);
	        button5.setVisibility(View.VISIBLE);
	        TextView06.setVisibility(View.VISIBLE);
	        TextView01.setVisibility(View.VISIBLE);
	        textView2.setVisibility(View.VISIBLE);
	        button6.setVisibility(View.VISIBLE);
	        edtdata.setText("");
	        button1.setVisibility(View.VISIBLE);
	        vermetodo=vermetodo + 2;
	        }
	}
	}
        //metodo para o botao sele��o de ano
	public void ano(View v){
	datacompara =(edtdata.getText().toString());
	datacomparatamanho = datacompara.length();
	
	if(datacomparatamanho < 10){
		Toast.makeText(getApplicationContext(), "Data inv�lida!", Toast.LENGTH_LONG).show();		
	}else{
        TextView06.setVisibility(View.VISIBLE);
        TextView01.setVisibility(View.VISIBLE);
        Banco_de_dados bd = new Banco_de_dados(this);
        bd.fragmento_consultaano(this);
        if(TextView01.getText().toString().equals("Consumo")){
        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
        TextView06.setVisibility(View.INVISIBLE);
        TextView01.setVisibility(View.INVISIBLE);
        }else{
        button4.setEnabled(false);
        button3.setEnabled(false);
        button2.setEnabled(false);
        button5.setVisibility(View.VISIBLE);
        TextView06.setVisibility(View.VISIBLE);
        TextView01.setVisibility(View.VISIBLE);
        textView2.setVisibility(View.VISIBLE);
        button6.setVisibility(View.VISIBLE);
        edtdata.setText("");
        vermetodo = vermetodo + 3;
        }
	}
	
	}
//metodo para o botao comparar data ano ou dia ou mes	
	public void comparar(View v){

	datacompara =(edtdata.getText().toString());
	datacomparatamanho = datacompara.length();
	if(datacomparatamanho < 10){
	Toast.makeText(getApplicationContext(), "Data inv�lida!", Toast.LENGTH_LONG).show();		
	}else{
	if(vermetodo == 1){	

	Banco_de_dados bd = new Banco_de_dados(this);
	bd.fragmento_consultadiacompara(this);
	if(TextView02.getText().toString().equals("Consumo")){
        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
        TextView05.setVisibility(View.INVISIBLE);
        TextView02.setVisibility(View.INVISIBLE);
        }else{
        TextView05.setVisibility(View.VISIBLE);
        TextView02.setVisibility(View.VISIBLE);
        textView3.setVisibility(View.VISIBLE);
        edtdata.setText("");
        textView5.setVisibility(View.VISIBLE);
        percentual();     
        }
		}
	if(vermetodo == 2){
	Banco_de_dados bd = new Banco_de_dados(this);
	bd.fragmento_consultamescompara(this);
	if(TextView02.getText().toString().equals("Consumo")){
        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
        TextView05.setVisibility(View.INVISIBLE);
        TextView02.setVisibility(View.INVISIBLE);
        }else{
        TextView05.setVisibility(View.VISIBLE);
        TextView02.setVisibility(View.VISIBLE);
        textView3.setVisibility(View.VISIBLE);
        edtdata.setText("");
        textView5.setVisibility(View.VISIBLE);
        percentual();
        }
	}
	if(vermetodo == 3){
	Banco_de_dados bd = new Banco_de_dados(this);
	bd.fragmento_consultaanocompara(this);
	if(TextView02.getText().toString().equals("Consumo")){
        Toast.makeText(getApplicationContext(), "Data n�o cadastrada",Toast.LENGTH_LONG).show();	
        TextView05.setVisibility(View.INVISIBLE);
        TextView02.setVisibility(View.INVISIBLE);
        }else{
        TextView05.setVisibility(View.VISIBLE);
        TextView02.setVisibility(View.VISIBLE);
        textView3.setVisibility(View.VISIBLE);
        edtdata.setText("");
        textView5.setVisibility(View.VISIBLE);
        percentual();
        }
	}
	}
	}
	//metodo para criar "grafico" entre duas datas 
	public void percentual(){
	String data = TextView06.getText().toString();
	data = data.substring(5);
	data1.setText(data);
	data = TextView05.getText().toString();
	data = data.substring(5);
	data2.setText(data);
	float total, porcemtagem1,porcemtagem2,valor1, valor2;
	valor1 =(Float.parseFloat(TextView01.getText().toString()));
	valor2 = (Float.parseFloat(TextView02.getText().toString()));
	total = valor1 + valor2;
	porcemtagem1 = (valor1 * 100) / total;
	porcemtagem2 = (valor2 * 100) / total;
	porcem1.setText(Float.toString(porcemtagem1).substring(0,5)+"%");
	porcem2.setText(Float.toString(porcemtagem2).substring(0,5)+"%");
	porcemtagem1 = porcemtagem1 /2;
	porcemtagem2 = porcemtagem2 /2;
	while(	porcemtagem1 >= 0){
	barra1.setText(barra1.getText().toString() + ".");	
	porcemtagem1 = 	porcemtagem1 - 1;	
	}
    while(	porcemtagem2 >= 0){
    barra2.setText(barra2.getText().toString() + ".");	
	porcemtagem2 = 	porcemtagem2 - 1;		
	}
    data1.setVisibility(View.VISIBLE);
    data2.setVisibility(View.VISIBLE);
    barra1.setVisibility(View.VISIBLE);
    barra2.setVisibility(View.VISIBLE);
    porcem1.setVisibility(View.VISIBLE);
    porcem2.setVisibility(View.VISIBLE);
	}
	//metodo para limpar a tela, ou seja, votando igua ao inicio  
	public void limpardados(View v){
		data1.setText("");
	    data2.setText("");
	    barra1.setText("");
	    barra2.setText("");
	    porcem1.setText("");
	    porcem2.setText("");
	    TextView01.setText("Consumo");
	    TextView02.setText("");
	    TextView06.setText("");
	    TextView02.setText("Consumo");
	    TextView05.setText("");
	    button2.setEnabled(true);
	    button4.setEnabled(true);
	    button3.setEnabled(true);
	    button1.setVisibility(View.GONE);
	    TextView05.setVisibility(View.GONE);
	    textView3.setVisibility(View.GONE);
	    textView5.setVisibility(View.GONE);
	    textView2.setVisibility(View.GONE);
	    button5.setVisibility(View.GONE);
	    TextView06.setVisibility(View.GONE);
	    TextView02.setVisibility(View.GONE);
	    TextView01.setVisibility(View.GONE);
	    button6.setVisibility(View.GONE);   
		data1.setVisibility(View.GONE);
	    data2.setVisibility(View.GONE);
	    barra1.setVisibility(View.GONE);
	    barra2.setVisibility(View.GONE);
	    porcem1.setVisibility(View.GONE);
	    porcem2.setVisibility(View.GONE);
	    vermetodo = 0;
	}
    //metodo para criar conta de agua, somente em sele��o de mes 	
    public void gerarconta(View v){ 
    	if(TextView02.getText().toString().equals("Consumo") || TextView02.getText().toString().isEmpty()){
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle("Simulador de conta"); 
            float valoragua = Float.parseFloat(TextView01.getText().toString());
            valoragua = valoragua / 1000;
            float aguam = valoragua;
            if( aguam < 11){
            	valoragua = (float) 20.64;
            }
            if(aguam > 10 && aguam < 21){
            	float valoramais = aguam - 10;
            	valoragua = (float) ((float) (valoramais * 3.23) + 20.64);
            }
             if(aguam > 20 && aguam < 31 ){
            	float valoramais = aguam - 10;
            	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
            }
            if(aguam > 30 && aguam < 51){
            	float valoramais = aguam -10;
            	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
            }
            if( aguam > 50){
            	float valoramais = aguam -10;
            	valoragua = (float) ((float) (valoramais * 8.89) + 20.64);
            } 
         
            float valortotal =  valoragua + valoragua;
   		    builder.setMessage("M�s: "+TextView06.getText().toString().substring(9) + " \n\nLitros: "+aguam+"m� \n\nValor: �gua R$"+ valoragua+" \n\nValor: Esgoto R$"+ valoragua+"\n ------------------------------ \nValor total: R$ "+valortotal+"\n\n OBS: N�o contando taxas realizadas pela SABESP;\n").show();	 	
    	     
    	}else{
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    							           switch (which){
    							           case DialogInterface.BUTTON_POSITIVE:
    							        	   primeira();
    							               break;
    							           case DialogInterface.BUTTON_NEGATIVE:
    							        	  segunda();     
    							               break;
    							           }
    							       }
    							   };           
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle("Simulador de conta");              
   		    builder.setMessage("Escolha a data que deseja simular a conta!")
   		    .setPositiveButton(TextView06.getText().toString().substring(6), dialogClickListener)
			.setNegativeButton(TextView05.getText().toString().substring(6), dialogClickListener).show();		                                 
   
    	}
    	 
	}
//metodo para caixa de texto primeira data 
	public void primeira(){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle("Simulador de conta"); 
            float valoragua = Float.parseFloat(TextView01.getText().toString());
            valoragua = valoragua / 1000;
            float aguam = valoragua;
            if( aguam < 11){
            	valoragua = (float) 20.64;
            }
            if(aguam > 10 && aguam < 21){
            	float valoramais = aguam - 10;
            	valoragua = (float) ((float) (valoramais * 3.23) + 20.64);
            }
             if(aguam > 20 && aguam < 31 ){
            	float valoramais = aguam - 10;
            	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
            }
            if(aguam > 30 && aguam < 51){
            	float valoramais = aguam -10;
            	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
            }
            if( aguam > 50){
            	float valoramais = aguam -10;
            	valoragua = (float) ((float) (valoramais * 8.89) + 20.64);
            } 
         
           
            float valortotal =  valoragua + valoragua;
   		    builder.setMessage("M�s: "+TextView06.getText().toString().substring(9) + " \n\nLitros: "+aguam+"m� \n\nValor: �gua R$"+ valoragua+" \n\nValor: Esgoto R$"+ valoragua+"\n\n -------------------------- \n\nValor total: R$ "+valortotal+"\n\n OBS: N�o contando taxas realizadas pela SABESP;\n").show();	 	
    
	}
//metodo para caixa de texto segunda data 
    public void segunda(){	
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setTitle("Simulador de conta"); 
        float valoragua = Float.parseFloat(TextView02.getText().toString());
        valoragua = valoragua / 1000;
        float aguam = valoragua;
        if( aguam < 11){
        	valoragua = (float) 20.64;
        }
        if(aguam > 10 && aguam < 21){
        	float valoramais = aguam - 10;
        	valoragua = (float) ((float) (valoramais * 3.23) + 20.64);
        }
         if(aguam > 20 && aguam < 31 ){
        	float valoramais = aguam - 10;
        	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
        }
        if(aguam > 30 && aguam < 51){
        	float valoramais = aguam -10;
        	valoragua = (float) ((float) (valoramais * 8.07) + 20.64);
        }
        if( aguam > 50){
        	float valoramais = aguam -10;
        	valoragua = (float) ((float) (valoramais * 8.89) + 20.64);
        }
      
        float valortotal =  valoragua + valoragua;
		    builder.setMessage("M�s: "+TextView05.getText().toString().substring(9) + " \n\nLitros: "+aguam+"m� \n\nValor: �gua R$"+ valoragua+" \n\nValor: Esgoto R$"+ valoragua+"\n\n -------------------------- \n\nValor total: R$ "+valortotal+"\n\n OBS: N�o contando taxas realizadas pela SABESP;\n").show();	 	
	}
	
    
	//metodo  para botao voltar
		@Override
		public void onBackPressed() {
			Intent it = new Intent(fragmento_consulta.this, Inicio.class);
			startActivity(it);
			fragmento_consulta.this.finish();
			super.onBackPressed();
		}
}
