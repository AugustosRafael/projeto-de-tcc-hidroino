package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.hidro.hidroino.R;

public class Aprendizado extends Activity {
        //cria��o das vari�veis 
	Button button5,button4,button3,button2,button1;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_aprendizado);
            //colocando cor no actionbar
	    ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));
            //identificadores dos botoes via id 
	    button5 =(Button)findViewById(R.id.button5);
	    button4 =(Button)findViewById(R.id.button4);
	    button3 =(Button)findViewById(R.id.button3);
	    button2 =(Button)findViewById(R.id.button2);
	    button1 =(Button)findViewById(R.id.button1);
	}
        //cria��o das vari�veis
	String title,message;
	int tipo=0;
	int cont =0;
        //metodo para outros 
	public void outros(View v){
	cont =0;	
	tipo = 1;
       //chamando metodo message	
	message();
    }
      //metodo para cozinha
	public void cozinha(View v){
	cont =0;
        tipo =2;
	message();
    }
      //metodo para banheiro
	public void banheiro(View v){
	cont =0;
	tipo = 3;
	message();
    }
        //metodo para lavan
	public void lavan(View v){
	cont =0;
	tipo = 4;
    message();
    }
       //metodo para quintal
	public void quintal(View v){
	cont =0;
	tipo =5 ;
	message();
    }
	//metodo message
	public void message(){		
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
						           switch (which){
						           case DialogInterface.BUTTON_POSITIVE:
						        	   proximo();
						               break;
						           case DialogInterface.BUTTON_NEGATIVE:
						        	  anterior();     
						               break;
						           }
						       }
						   };              
						   AlertDialog.Builder builder = new AlertDialog.Builder(this);
		                        builder.setIcon(R.drawable.ic_launcher);
		                        if(tipo == 1){
		                        	title = "Outros";
		                                  if(cont == 0){
		                                   message = "Aproveite a �gua da chuva para tarefas que n�o necessitem de �gua pot�vel";
		                                  }
		                                    if(cont == 1){
		                                   message = "Certifique-se de que as torneiras n�o est�o com vazamentos e que n�o fiquem gotejando";
		                                   }
		                                    if(cont == 2){
		                                   message = "Regue plantas com regador ao inv�s de mangueiras e procure regar ao amanhecer ou ao anoitecer para evitar o desperd�cio relacionado � evapora��o";
		                                    }
		                                    if(cont == 3){
				                            message = "N�o deixe a caixa d��gua transbordar e mantenha-a tampada";
				                            }
		                                 
		                        }
		                        if(tipo == 2){
		                        	title = "Cozinha";
		                                  if(cont == 0){
		                                   message = "Ao lavar a lou�a, feche a torneira e a abra apenas para o enxague";
		                                  }
		                                    if(cont == 1){
		                                   message = "Deixe pratos e talheres de molho antes de lav�-los";
		                                   }
		                                    if(cont == 2){
		                                   message = "Limpe bem os res�duos dos pratos e panelas e jogue-os no lixo antes da lavagem";
		                                    }
		                                    if(cont == 3){
				                            message = "Caso utilize uma lava-lou�as, procure deixar atingir a capacidade m�xima antes iniciar a lavagem";
				                            }
		                                 
		                        }
		                        if(tipo == 3){
		                        	title = "Banheiro";
		                                  if(cont == 0){
		                                   message = "Reduza o tempo de banho e feche o registro ao se ensaboar";
		                                  }
		                                    if(cont == 1){
		                                   message = "Feche a torneira ao escovar os dentes e enxague a boca utilizando um copo";
		                                   }
		                                    if(cont == 2){
		                                   message = "Utilize um balde ou bacia para reutilizar a �gua do banho e de pias";
		                                    }
		                                    if(cont == 3){
				                            message = "N�o jogue lixo vaso sanit�rio";
				                            }
		                                 
		                        }
		                        if(tipo == 4){
		                        	title = "Lavanderia";
		                                  if(cont == 0){
		                                   message = "Acumule o m�ximo de roupas poss�vel antes da lavagem";
		                                  }
		                                    if(cont == 1){
		                                   message = "Reutilize a �gua usada na lavagem de roupas para lavar o quintal ou o carro";
		                                   }
		                                    if(cont == 2){
		                                   message = "N�o deixe a torneira aberta por muito tempo";
		                                    }
		                                    if(cont == 3){
				                            message = "Enxague o m�ximo de roupa poss�vel de uma vez";
				                            }
		                                 
		                        }
		                        if(tipo == 5){
		                        	title = "Quintal";
		                                  if(cont == 0){
		                                   message = "Ao lavar o carro, utilize balde e pano ao inv�s da mangueira";
		                                  }
		                                    if(cont == 1){
		                                   message = "Utilize a vassoura para limpar o quintal, ao inv�s de �gua pot�vel";
		                                   }
		                                    if(cont == 2){
		                                   message = "Caso for usar a magueira, n�o usar por muito tempo";
		                                    }
		                                    if(cont == 3){
				                            message = "Usar �gua da chuva para regar as plantas";
				                            }
		                                 
		                        }
		                   builder.setTitle(title);              
						   builder.setMessage(message)
						   .setPositiveButton("Proximo", dialogClickListener)
						   .setNegativeButton("Anterior", dialogClickListener).show();		                                 
	}
        //metodo para proximo para botao na caixa de texto
	 public void proximo(){
         cont = cont + 1;
         if(cont < 4  && cont >=0 ){
        	  message(); 
         }       
         }
        //metodo para anterior para botao na caixa de texto
        public void anterior(){
            cont = cont - 1;
       if(cont < 4  && cont >=0 ){
     	  message(); 
      }
       if(cont < 0 ){
    	cont =0;   
       }
        }
	//metodo  para botao voltar
	@Override
	public void onBackPressed() {
		Intent it = new Intent(Aprendizado.this, Inicio.class);
		startActivity(it);
		Aprendizado.this.finish();
		super.onBackPressed();
	}
	
}


