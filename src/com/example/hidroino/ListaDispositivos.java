package com.example.hidroino;

import java.util.Set;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListaDispositivos extends ListActivity{
	
     //variavel do tipo BluetoothAdapter
	private BluetoothAdapter bluetooth;
	
	//variavel do tipo String
	 static String Endereco_MAC = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//lista para mostrar os dipositivos para se conectar
		ArrayAdapter<String> arraybluetooth = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		
		//obtem o Adapter Bluetooth local
		bluetooth = BluetoothAdapter.getDefaultAdapter();
		
		//criando um BluetoothDevice dsps=dispositivos pareados
		Set<BluetoothDevice> dsps = bluetooth.getBondedDevices();
		
		if(dsps.size()>0){			
		     for(BluetoothDevice dispositivo : dsps ) {
		     String nomeaparelho = dispositivo.getName();
		     String macaparelho = dispositivo.getAddress();
		     arraybluetooth.add(nomeaparelho + " \n " + macaparelho);		     
		     }
	    }
		setListAdapter(arraybluetooth);
	}


	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		String infogeral = ((TextView) v).getText().toString();
		
		String endereco = infogeral.substring(infogeral.length() -17);
		
		Intent retornaMac = new Intent();
		retornaMac.putExtra(Endereco_MAC , endereco);
		
		setResult(RESULT_OK,retornaMac);
		finish();
	}

}

