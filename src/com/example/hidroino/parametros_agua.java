package com.example.hidroino;

public class parametros_agua {
	//ccriando variaveis publicas
		public String data;
		public String agua;
		
		    //COnstrutor para a classe parametros_agua
			public parametros_agua(){ }
			//COnstrutor para a classe parametros_agua com atributos
			public parametros_agua(String data, String agua){ 
		    //construindo parametros para as variaveis
			this.data = data;
			this.agua = agua;
			}
			public String getData() {
				return data;
			}
			public void setData(String data) {
				this.data = data;
			}
			public String getAgua() {
				return agua;
			}
			public void setAgua(String agua) {
				this.agua = agua;
			}
			@Override
			public String toString() {
				return "Data: " + data + "\nConsumo: " + agua ;
			}


}

