package com.example.hidroino;

public class parametros_configuração {
	
	//ccriando variaveis publicas
	public String nome;
	public int total_pessoas;
	public int limite_agua;
	
	    //COnstrutor para a classe parametros_configuração
		public parametros_configuração(){ }
		//COnstrutor para a classe parametros_configuração com atributos
		public parametros_configuração(String nome, int total_pessoas, int limite_agua){ 
	    //construindo parametros para as variaveis
		this.nome=nome;
		this.total_pessoas=total_pessoas;
		this.limite_agua=limite_agua;
		}
	
	//construindo os getters e setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getTotal_pessoas() {
		return total_pessoas;
	}
	public void setTotal_pessoas(int total_pessoas) {
		this.total_pessoas = total_pessoas;
	}
	public int getLimite_agua() {
		return limite_agua;
	}
	public void setLimite_agua(int limite_agua) {
		this.limite_agua = limite_agua;
	}
	
	//metodo para gerar strings para as variaveis
	@Override
	public String toString() {
		return "parametros_configuração [nome=" + nome + ", total_pessoas="
				+ total_pessoas + ", limite_agua=" + limite_agua + "]";
	}
	
}
