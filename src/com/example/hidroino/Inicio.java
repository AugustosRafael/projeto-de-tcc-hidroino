package com.example.hidroino;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import at.abraxas.amarino.Amarino;
import at.abraxas.amarino.AmarinoIntent;
import com.hidro.hidroino.R;

public class Inicio extends Activity{

	//variavel para validar osalvamento no banco de dados
	int validar;
    //criando variaveis para os botoes
	Button btnajuda,btnconectar,btnaprendizado,btnconfiguracao,btnpesquisa, btnsalvar,
	button6,button7;
	//criando variaveis para os TexteView
	TextView txtgastodelitros1,txtgastodelitros2,txtgastodelitros3,txtgastodelitros4,
	txtgastodelitros5, txtlitros, txtdata;
	//variavel para contador de imagens 
	int cont = 0;
	//variaveis para uso relacionado com bluetooh
	private static final int SOLICITA_ATIVACAO =1;
	private static final int SOLICITA_CONEXAO=2;
	//variavel do tipo BluetoothAdapter, para as intera�oes bluetooh
	private BluetoothAdapter bluetooth = null;
	//string para capiturar o endere�o mac
	private static String MAC = null;
	//variavel boolean para identificar o estado da conexao
	boolean conexao = false;
	//identificador para a classe StatusAmarino
	private StatusAmarino statusamarino = new StatusAmarino();
	//passando valor de um txt para uma string 
	//variavel para ver bluetooth 
	int contb = 0;
	
	//metodo onclick para botao conectar
	public void onclickblue(View v){ 
		MediaPlayer mp = MediaPlayer.create(this,R.raw.gota);
		mp.start();	
    if(contb == 0){
    	bluetooth = BluetoothAdapter.getDefaultAdapter();
		if(bluetooth == null){
		    Toast.makeText(getApplicationContext(), "Desculpe!! mas seu dispositivo n�o possui bluetooh", Toast.LENGTH_LONG).show();	
		}
		if(!bluetooth.isEnabled()){
			 Intent ativar = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			 startActivityForResult(ativar, SOLICITA_ATIVACAO);
		}
    	button6.setVisibility(View.VISIBLE);
    	button7.setVisibility(View.VISIBLE);
        contb = contb - 1;
    }else{
    	button6.setVisibility(View.GONE);
    	button7.setVisibility(View.GONE);
    	contb = contb + 1;
    }
	
	}
	//metodo para conectar
	public void onclickbluec(View v){
		Intent abrirlista = new Intent(Inicio.this, ListaDispositivos.class);
		startActivityForResult(abrirlista, SOLICITA_CONEXAO);	
	}
	public void onclickblued(View v){  
	if(conexao != false){
	Amarino.disconnect(Inicio.this, MAC);
	Toast.makeText(getApplicationContext(), "Bluetooth foi desconectado com sucesso!!", Toast.LENGTH_LONG).show();
	conexao = false;
	}
	}
	//metodo caso saia da actvity
	@Override
	protected void onStop() {
		Amarino.disconnect(Inicio.this, MAC);
		super.onStop();
	}
		
	//metodo onclick para botao configura��o
		public void onclickconf(View v){
			MediaPlayer mp = MediaPlayer.create(this,R.raw.gota);
			mp.start();	
			   Intent it = new Intent(Inicio.this, Configuracao.class);
			   startActivity(it);
			   Inicio.this.finish();
		}
		//metodo onclick para botao consulta
		public void onclickcons(View v){
			MediaPlayer mp = MediaPlayer.create(this,R.raw.gota);
			mp.start();	
			   Intent it = new Intent(Inicio.this, fragmento_consulta.class);
			   startActivity(it);
			   Inicio.this.finish();
		}
		//metodo onclick para botao aprendizado
		public void onclickapre(View v){
			MediaPlayer mp = MediaPlayer.create(this,R.raw.gota);
			mp.start();	
			   Intent it = new Intent(Inicio.this, Aprendizado.class);
			   startActivity(it);
			   Inicio.this.finish();
		}
		//metodo onclick para botao ajuda
		public void onclickajud(View v){
			MediaPlayer mp = MediaPlayer.create(this,R.raw.gota);
			mp.start();	
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
		 	//saindo imagem que se chama gota
	    	imageView.setImageResource(R.drawable.gota);
	    	//estado do imagemView com visivel
	    	imageView.setVisibility(View.VISIBLE);
	    	//contador para ver qual imagem vai ser mostrada
	    	cont=0;
		}
     //metodo onclick para imagem
	public void onclickima(View v){
		if(cont == 0){
	 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
    	imageView.setImageResource(R.drawable.gota1);   	
		}
		if(cont == 1){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota2);   	
			}
		if(cont == 2){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota3);   	
			}
		if(cont == 3){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota4);   	
			}
		if(cont == 4){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota5);   	
			}
		if(cont == 5){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota6);   	
			}
		if(cont == 6){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota7);   	
			}
		if(cont == 7){
		 	ImageView imageView = (ImageView) findViewById(R.id.imageView2);
	    	imageView.setImageResource(R.drawable.gota8);   	
			}
		
		if(cont == 8){
			ImageView imageView = (ImageView) findViewById(R.id.imageView2);
			//estado do imagemView com desaparecer
			imageView.setVisibility(View.GONE);			
		}	
		//somando contador para condi��es
		cont=cont+1;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_inicio);
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));
	    
		//Pegando data do celular e setando no txtdata
		txtdata = (TextView) findViewById(R.id.txtdata);
		long date = System.currentTimeMillis(); 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = sdf.format(date);
		txtdata.setText(dateString);
		
				
	//criando identificadores para os componentes
	btnconectar = (Button)findViewById(R.id.button2);
	btnajuda = (Button)findViewById(R.id.button1);
	btnaprendizado = (Button)findViewById(R.id.button5);
	btnconfiguracao = (Button)findViewById(R.id.button3);
	btnsalvar = (Button)findViewById(R.id.btnsalvar);
	btnpesquisa = (Button)findViewById(R.id.button4);
	txtgastodelitros4 = (TextView)findViewById(R.id.txtgastodelitros4);
	txtgastodelitros2 = (TextView)findViewById(R.id.txtgastodelitros2);
	txtlitros = (TextView)findViewById(R.id.txtlitros);
	txtdata = (TextView)findViewById(R.id.txtdata);
	btnsalvar = (Button)findViewById(R.id.btnsalvar);
	button6 = (Button)findViewById(R.id.button6);
	button7 = (Button)findViewById(R.id.button7);
	//passando limite e consumo diario para Textview
	Banco_de_dados db = new Banco_de_dados(this);
	db.selectinicio(this);
	db.selectinicio2(this);
	
	//estado dos botoes conectar e desconectar
	button6.setVisibility(View.GONE);
	button7.setVisibility(View.GONE);
	
}	
	//metodo onActivityresult
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch(requestCode){
	    case SOLICITA_ATIVACAO:
	    if(resultCode == Activity.RESULT_OK){
	    	Toast.makeText(getApplicationContext(), "Bluetooth foi ativado com sucesso!!", Toast.LENGTH_LONG).show();	
	    }else{
	    	Toast.makeText(getApplicationContext(), "Bluetooth foi ativado com falha!!", Toast.LENGTH_LONG).show();
	    	//finish();
	    }
	    break;
	    case SOLICITA_CONEXAO:
	    	if(resultCode == Activity.RESULT_OK){	
	        MAC= data.getExtras().getString(ListaDispositivos.Endereco_MAC);
	        
	        registerReceiver(statusamarino, new IntentFilter(AmarinoIntent.ACTION_CONNECTED));	
	        
		    Amarino.connect(Inicio.this, MAC);
		    conexao = true;
		    }else{
		    	Toast.makeText(getApplicationContext(), "Falha ao tentar pareamento", Toast.LENGTH_LONG).show();
		    }	
	    break;
	}	    
	}
	//classe para ver status do amarino e mandar dados
	public class StatusAmarino extends BroadcastReceiver{
		String litros;
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			final String action = intent.getAction();			
			if(AmarinoIntent.ACTION_CONNECTED.equals(action)){
				conexao = true;
				Toast.makeText(getApplicationContext(), "Conex�o feita com sucesso", Toast.LENGTH_LONG).show();	
			    registerReceiver(statusamarino, new IntentFilter(AmarinoIntent.ACTION_RECEIVED));				
			}else if(AmarinoIntent.ACTION_CONNECTION_FAILED.equals(action)){
				conexao = false;
				Toast.makeText(getApplicationContext(), "Conex�o feita com falha", Toast.LENGTH_LONG).show();
			}
			if(AmarinoIntent.ACTION_RECEIVED.equals(action)){
			 final int tipodados = intent.getIntExtra(AmarinoIntent.EXTRA_DATA_TYPE, -1);
      		 switch(tipodados){
			 case AmarinoIntent.STRING_EXTRA:	  
			  String dados = intent.getStringExtra(AmarinoIntent.EXTRA_DATA);			  			
			  txtlitros.setText(dados);
			  break;			
			  }
			}			
			}
		}
	//metodo para salvar litros 
    public void onclicksalv(View view){ 
    	if(txtlitros.getText().toString().equals("Aguardando...") || txtlitros.getText().toString().isEmpty() || conexao == false){
    		if(txtlitros.getText().toString().equals("Aguardando...") || txtlitros.getText().toString().isEmpty()){
    			Toast.makeText(getApplicationContext(), "Receba o consumo do equipamento para poder salvar os dados", Toast.LENGTH_LONG).show();			 	
    		}else{
    			Toast.makeText(getApplicationContext(), "Esteja conectado com o equipamento para poder salvar os dados", Toast.LENGTH_LONG).show();			 	   		
    		}
     	}else{
        Banco_de_dados bd = new Banco_de_dados(this);
        parametros_agua pa = new parametros_agua();
        pa.setData(txtdata.getText().toString());       
        pa.setAgua(txtlitros.getText().toString());       
        bd.insertagua(pa);
     	bd.selectinicio2(this);
        double gastos = Double.parseDouble(txtgastodelitros2.getText().toString());
        int limite = Integer.parseInt(txtgastodelitros4.getText().toString());
        int porcentagem = 0;
 		porcentagem = (int) ((gastos * 100)/limite);						
 		NotificationManager nm=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
  		PendingIntent p =PendingIntent.getActivity(this,0, new Intent(this, notificacao.class), 0);	
  		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
  		if(porcentagem < 71 ){
  		builder.setTicker("Hidroino");
  		builder.setContentTitle("Voc� consumiu");
  		builder.setContentText(porcentagem+"% do seu limite de " + limite + "L");
  		builder.setSmallIcon(R.drawable.bolinha_ve);
  		builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher));
 		}
  		if(porcentagem > 71 && porcentagem < 91 ){
  	 		builder.setTicker("Hidroino");
  	 		builder.setContentTitle("Voc� consumiu");
  	  		builder.setContentText(porcentagem+"% do seu limite de " + limite+ "L");
  	 		builder.setSmallIcon(R.drawable.bolinha_a);
  	 		builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher));
  			}
  		if(porcentagem > 91 ){
  	 		builder.setTicker("Hidroino");
  	 		builder.setContentTitle("Voc� consumiu");
  	  		builder.setContentText(porcentagem+"% do seu limite de " + limite+"L");
  	 		builder.setSmallIcon(R.drawable.bolinha_v);
  	 		builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher));
  			}
  		Notification n = builder.build();
  		n.vibrate = new long[]{150, 300, 150,600};
  		nm.notify(R.drawable.ic_launcher, n);
  		
  		try{
  			Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
  			Ringtone toque = RingtoneManager.getRingtone(this, som);
  			toque.play();
  		}
  		catch(Exception e){}
    Amarino.sendDataToArduino(Inicio.this, MAC, 'A', 1);
    }
    }
      
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	 //Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inicio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	*/
}
