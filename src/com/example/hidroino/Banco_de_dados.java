package com.example.hidroino;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class Banco_de_dados extends SQLiteOpenHelper{
	// nome e vers�o do banco de dados
    private static final String Nome_base = "Banco"; 
    private static final int Versao_base = 1; 

	//cria o banco de dados
	public Banco_de_dados(Context context) {
		super(context, Nome_base, null, Versao_base);
	}
	
    //metodo para criar as tabelas do banco
	@Override
	public void onCreate(SQLiteDatabase db) {
	// criando e rodando tabela configura��o
	String Tabela_configuracao = "CREATE TABLE IF NOT EXISTS configuracao("
			    + "nome TEXT,"
			    + "total_pessoas INTEGER,"
			    + "limite_agua INTEGER"
				+"); ";
		 
	String	 Tabela_con = "CREATE TABLE IF NOT EXISTS con("
			      + "data TEXT,"
			      + "agua TEXT"
			      +");";
		        db.execSQL(Tabela_configuracao); 
		        db.execSQL(Tabela_con); 
	}
	


	//metodo onUpgrade banco(update do banco "1 para 2 ")
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {		
		//exclui as tabelaa 
		String Tabela_configuracao = ("DROP TABLE configuracao");
		String Tabela_consulta = ("DROP TABLE con");
		//rodando
		db.execSQL(Tabela_configuracao);
		db.execSQL(Tabela_consulta);
		//apos excluir tenho que criar de novo a tabela
		onCreate(db);
	}
	 public void insertagua(parametros_agua pa){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("data", pa.getData());
		cv.put("agua", pa.getAgua());
		db.insert("con", null, cv);
		db.close();
		}
	 
	 public List<parametros_agua> selectCosumo(){
			List<parametros_agua> Listconsumo = new ArrayList<parametros_agua>(); 			
			SQLiteDatabase db = getReadableDatabase();
			//String data = "SELECT data FROM con";

			String dados = "SELECT data,SUM(agua) AS'agua' FROM con";

			Cursor c = db.rawQuery(dados, null);	
			parametros_agua pa = new parametros_agua();	
            
			if(c.moveToFirst()){
			do{ 
				 pa.setData(c.getString(0));
			     pa.setAgua(c.getString(1));
		     Listconsumo.add(pa);
		     }while(c.moveToNext());			
			 }	
			
			 db.close();  	 
			 return Listconsumo;		
			}
	 
	//metodo inserir dados
	public void insertconfiguracao(parametros_configura��o pc){	
	SQLiteDatabase db = getWritableDatabase();
	ContentValues cv = new ContentValues();
	cv.put("nome", pc.getNome());
	cv.put("total_pessoas", pc.getTotal_pessoas());
	cv.put("limite_agua", pc.getLimite_agua());
	db.insert("configuracao", null, cv);
	db.close();
	}
	
	//metodo inserir dados no EDIT na tela de configura��o
	public void selectconfiguracao(Configuracao co){
		SQLiteDatabase db= getReadableDatabase();
		String sqlSelectConfiguracao = "SELECT * FROM configuracao";		
		Cursor c = db.rawQuery(sqlSelectConfiguracao, null);
		String nome; 
		int limite,pessoa;
		if(c.moveToFirst()){		
		nome=(c.getString(0));
		pessoa=(c.getInt(1));
		limite=(c.getInt(2));
		if(nome.isEmpty()){
		co.edtnome.setEnabled(true);
		co.edtlimite.setEnabled(true);
		co.edtpessoa.setEnabled(true);
		}else{	
	    //comvertendo variaveis para saida nos edts
		String pes =Integer.toString(pessoa);
		String lim =Integer.toString(limite);
		//setando valores nos edtis	     	
		co.edtnome.setText(nome);
		co.edtpessoa.setText(pes);
		co.edtlimite.setText(lim);
		co.edtnome.setEnabled(false);
		co.edtlimite.setEnabled(false);
		co.edtpessoa.setEnabled(false);
		co.btns.setEnabled(false);
		co.btne.setEnabled(true);
		}		
		}			
		db.close();		
	    }

    //metodo para botao editar
	public void editardados(){
		SQLiteDatabase db = getReadableDatabase();
		String deletetabela = ("DROP TABLE configuracao");
		db.execSQL(deletetabela);
	    onCreate(db);
	}
	
	//metodo inserir dados no TextView (gasto de limite) na tela inicio
	public void selectinicio(Inicio in){
		SQLiteDatabase db= getReadableDatabase();
		String sqlselectconfiguracao = "SELECT * FROM configuracao";			
		Cursor c = db.rawQuery(sqlselectconfiguracao, null);
		String nome; 
		int limite,pessoa;
		if(c.moveToFirst()){		
		nome=(c.getString(0));
		pessoa=(c.getInt(1));
		limite=(c.getInt(2));	
		if(nome.equals(" ")){
		in.txtgastodelitros4.setText(" 0L ");
		}else{	
	    //comvertendo variaveis para saida nos edts
		String lim =Integer.toString(limite);
		//setando valor no textview    	
		in.txtgastodelitros4.setText( lim );
		}		
		}
		
		
		db.close();		
	    }   
	
public void selectinicio2(Inicio in){
	SQLiteDatabase db = getReadableDatabase();
	String data = in.txtdata.getText().toString();
	String agua = "SELECT SUM(agua) AS'agua' FROM con WHERE data = '"+data+"' GROUP BY data";

	//float ag = (float) 0.0000;	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){
	do{			
	String ag1 = (c.getString(0));
	/*ag1 = ag1.replaceAll(",", ".");
	ag = ag +(float) Float.parseFloat(ag1);
    NumberFormat format = NumberFormat.getInstance();  
	format.setMaximumFractionDigits(2);  
	format.setMinimumFractionDigits(1);  
	format.setMaximumIntegerDigits(10);  
	format.setRoundingMode(RoundingMode.HALF_UP);  
	ag = Float.valueOf(format.format(ag));*/ 
	in.txtgastodelitros2.setText(ag1);
	}while(c.moveToNext());
	}
	
	db.close();		
    }  
public void fragmento_consultadia(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String agua = "SELECT data,SUM(agua) AS'agua' FROM con WHERE data = '" + data + "' GROUP BY data";	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){
		
	do{			
	String edtdata = (c.getString(0));
	String edtagua = (c.getString(1));	
	if(edtagua.equals("")){	
	}else{
	fc.TextView01.setText(edtagua);
	fc.TextView06.setText("Data:"+edtdata);
	}
	}while(c.moveToNext());
	}
	db.close();		
    }
public void fragmento_consultadiacompara(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String agua = "SELECT data,SUM(agua)AS'agua' FROM con WHERE data = '" + data + "' GROUP BY data";	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){		
	do{			
	String edtdata = (c.getString(0));
	String edtagua = (c.getString(1));	
	if(edtagua.equals("")){	
	}else{
	fc.TextView02.setText(edtagua);
	fc.TextView05.setText("Data:"+edtdata);
	}
	}while(c.moveToNext());
	}
	db.close();		
    }  

public void fragmento_consultames(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String pedacodatainicio = (data.substring(2,10));
	String agua = "SELECT data,SUM(agua) FROM con WHERE data  LIKE '%"+pedacodatainicio+"'";	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){
		
	do{
    String edtdata = (c.getString(0));
	String edtagua = (c.getString(1));	
	try {
		if(edtagua.equals("")){	
		}else{
		fc.TextView01.setText(edtagua);
		fc.TextView06.setText("Data: m�s "+edtdata.substring(3,10));
		}	
	} catch (Exception e) {
		fc.TextView01.setText("Consumo");
	}
	
	}while(c.moveToNext());
	}
	db.close();		
    }
public void fragmento_consultamescompara(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String pedacodatainicio = (data.substring(2,10));
	String agua = "SELECT data,SUM(agua) FROM con WHERE data  LIKE '%"+pedacodatainicio+"'";	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){		
		do{	
		try {	
	    String edtdata = (c.getString(0));
		String edtagua = (c.getString(1));	
		if(edtagua.equals("")){	
		}else{
		fc.TextView02.setText(edtagua);
		fc.TextView05.setText("Data: m�s "+edtdata.substring(3,10));
		}
		} catch (Exception e) {
			fc.TextView02.setText("Consumo");
		}
		}while(c.moveToNext());
		}
		db.close();		
    }

  


public void fragmento_consultaano(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String pedacodatainicio = (data.substring(6,10));
	String agua = "SELECT SUM(agua) FROM con WHERE data  LIKE '%"+pedacodatainicio+"'";	
	Cursor c = db.rawQuery(agua, null);
	if(c.moveToFirst()){
		
		do{	
	    try {
		String edtagua = (c.getString(0));	
		if(edtagua.equals("")){	
		}else{
		fc.TextView01.setText(edtagua);
		fc.TextView06.setText("Data: ano "+data.substring(6,10));
		}
	    } catch (Exception e) {
			fc.TextView01.setText("Consumo");
		}
		}while(c.moveToNext());
		}
		db.close();			
    }

public void fragmento_consultaanocompara(fragmento_consulta fc){
	SQLiteDatabase db = getReadableDatabase();
	String data = fc.edtdata.getText().toString();
	String pedacodatainicio = (data.substring(6,10));
	String agua = "SELECT SUM(agua) FROM con WHERE data  LIKE '%"+pedacodatainicio+"'";	
	Cursor c = db.rawQuery(agua, null);
if(c.moveToFirst()){	
		do{	
			try{
		String edtagua = (c.getString(0));	
		if(edtagua.equals("")){	
		}else{
			fc.TextView02.setText(edtagua);
			fc.TextView05.setText("Data: ano "+data.substring(6,10));
		}
		}catch (Exception e) {
			fc.TextView02.setText("Consumo");
		}
		}while(c.moveToNext());
		}
		db.close();	
	
    }
}

