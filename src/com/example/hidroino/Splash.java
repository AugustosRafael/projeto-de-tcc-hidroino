package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import com.hidro.hidroino.R;

public class Splash extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		  
		setContentView(R.layout.tela_splash);	
		Context context = getApplicationContext();
		//construindo SharedPreferences
		SharedPreferences sharedPref = context.getSharedPreferences("Cadastrado", Context.MODE_PRIVATE);
		boolean cadastro = sharedPref.getBoolean("valor", false);
		if(cadastro == true){
	final ImageView imagemview = (ImageView)findViewById(R.id.imageView1);
	final int Duracao_tela = 2500; //tempo para splash
	
	new Handler().postDelayed(new Runnable() {			
				@Override
				public void run() {				
						Intent it = new Intent(Splash.this, Inicio.class);
						Splash.this.startActivity(it);
						Splash.this.finish();										
				}		
	},Duracao_tela);
	}else{
		final ImageView imagemview = (ImageView)findViewById(R.id.imageView1);
		final int Duracao_tela = 2500;
		new Handler().postDelayed(new Runnable() {			
					@Override
					public void run() {
						Intent it = new Intent(Splash.this, Tutorial.class);
						Splash.this.startActivity(it);
						Splash.this.finish();								
					}		
		},Duracao_tela);	
	}			
	}		
}
