package com.example.hidroino;

public class parametros_simulador_chuveiro {
	//ccriando variaveis publicas
			public String vazao;
			public String agua;
			
			    //COnstrutor para a classe parametros_agua
				public parametros_simulador_chuveiro(){ }
				//COnstrutor para a classe parametros_agua com atributos
				public parametros_simulador_chuveiro(String vazao, String agua){ 
			    //construindo parametros para as variaveis
				this.vazao = vazao;
				this.agua = agua;
				}
				public String getVazao() {
					return vazao;
				}
				public void setVazao(String vazao) {
					this.vazao = vazao;
				}
				public String getAgua() {
					return agua;
				}
				public void setAgua(String agua) {
					this.agua = agua;
				}
				@Override
				public String toString() {
					return "Data: " + vazao + "\nConsumo: " + agua ;
				}


}
