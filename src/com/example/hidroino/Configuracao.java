package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.hidro.hidroino.R;

public class Configuracao extends Activity {
	//variaveis para os EditsTexts
	EditText edtnome,edtpessoa,edtlimite;
	Button btns,btne;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_configuracao);
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));
	    
		//identificadores para os edits
		edtnome = (EditText)findViewById(R.id.edtnome);
		edtpessoa = (EditText)findViewById(R.id.edtpessoa);
		edtlimite = (EditText)findViewById(R.id.edtlimite);
		//identificadores para os botoes 
	    btns = (Button)findViewById(R.id.btnsalvar);
	    btne = (Button)findViewById(R.id.btneditar);
        //estado dos botoes
	    btns.setEnabled(true);
		btne.setEnabled(false);
	    
	    //contrutor para classe do bd
	     Banco_de_dados db = new Banco_de_dados(this);
	    //rodando metodo passar dados para o edit (selectconfiguracao)
	      db.selectconfiguracao(this);	    
	}
	
	//metodo  para botao voltar	
	@Override
	public void onBackPressed() {
		Intent it = new Intent(Configuracao.this, Inicio.class);
		startActivity(it);
		Configuracao.this.finish();
		super.onBackPressed();
	}				
   //metodo onclick para botao salvar
		public void onclicksalv(View v){				
			String teste_nome, teste_limite, teste_pessoa;
			teste_nome =(edtnome.getText().toString());
			teste_limite =(edtlimite.getText().toString());
			teste_pessoa =(edtpessoa.getText().toString());
		    if(teste_nome.equals("") || teste_limite.equals("") || teste_pessoa.equals("")){
		    	Toast.makeText(getApplicationContext(), "Por favor n�o deixe nenhum campo em branco!",Toast.LENGTH_LONG).show();
		    }else{	    	
		    int agua,pessoa,limite,lf;
			String nome;
		    nome =(edtnome.getText().toString());
			agua =(Integer.parseInt(edtlimite.getText().toString()));
			pessoa =(Integer.parseInt(edtpessoa.getText().toString()));
	              
	        if(agua == 0 || pessoa == 0){
	        	Toast.makeText(getApplicationContext(), "Por favor informe dados reais!",Toast.LENGTH_LONG).show();	
	        }else{
	        lf = agua /pessoa;
	        if(lf <= 110 ){
		        parametros_configura��o pa_co = new parametros_configura��o();
		        pa_co.setNome(edtnome.getText().toString()); 
		        pa_co.setTotal_pessoas(Integer.parseInt(edtpessoa.getText().toString()));  
		        pa_co.setLimite_agua(Integer.parseInt(edtlimite.getText().toString())); 	   		
		   		Banco_de_dados B_D_D = new Banco_de_dados(this);
		   		B_D_D.insertconfiguracao(pa_co);
		   		Toast.makeText(getApplicationContext(), "Dados salvos com sucesso!", Toast.LENGTH_LONG).show();
		   		btns.setEnabled(false);
				btne.setEnabled(true);
				edtnome.setEnabled(false);
				edtlimite.setEnabled(false);
				edtpessoa.setEnabled(false);
				B_D_D.selectconfiguracao(this);
			}else{
				 DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int which) {
				           switch (which){
				           case DialogInterface.BUTTON_POSITIVE:
				        	   diminuir();
				               break;
				           case DialogInterface.BUTTON_NEGATIVE:
				        	   manter();     
				               break;
				           }
				       }
				   };              
				   AlertDialog.Builder builder = new AlertDialog.Builder(this);
				   builder.setIcon(R.drawable.ic_launcher);
				   builder.setTitle("Configura��o");
				   builder.setMessage("O limite de �gua est� maior do que o recomendado pela ONU, que � 110 Litros de �gua por pessoa. Deseja diminuir ou continuar com o atual?")
				   .setPositiveButton("Diminuir", dialogClickListener)
				   .setNegativeButton("Manter", dialogClickListener).show();
				  }
	        }	        
		    }
		    
			}

		// metodo diminuir para caixa de texto	
		 public void diminuir(){
			 int agua,pessoa,lf;
			    agua =(Integer.parseInt(edtlimite.getText().toString()));
				pessoa =(Integer.parseInt(edtpessoa.getText().toString()));
				lf = 110 * pessoa;
      	        parametros_configura��o pa_co = new parametros_configura��o();
	   	        pa_co.setNome(edtnome.getText().toString()); 
	   	        pa_co.setTotal_pessoas(Integer.parseInt(edtpessoa.getText().toString()));  
	   	        pa_co.setLimite_agua(lf); 	   		
	   	   		Banco_de_dados B_D_D = new Banco_de_dados(this);
	   	   		B_D_D.insertconfiguracao(pa_co);
	        	Toast.makeText(getApplicationContext(), "Dados salvos com sucesso! O limite passou a ser "+ lf, Toast.LENGTH_LONG).show();
	        	btns.setEnabled(false);
				btne.setEnabled(true);
				edtnome.setEnabled(false);
				edtlimite.setEnabled(false);
				edtpessoa.setEnabled(false);
				B_D_D.selectconfiguracao(this);
         }
              // metodo manter para caixa de texto	
		 public void manter(){
	      	   parametros_configura��o pa_co = new parametros_configura��o();
		   	        pa_co.setNome(edtnome.getText().toString()); 
		   	        pa_co.setTotal_pessoas(Integer.parseInt(edtpessoa.getText().toString()));  
		   	        pa_co.setLimite_agua(Integer.parseInt(edtlimite.getText().toString())); 	   		
		   	   		Banco_de_dados B_D_D = new Banco_de_dados(this);
		   	   		B_D_D.insertconfiguracao(pa_co);
		        	Toast.makeText(getApplicationContext(), "Dados salvos com sucesso!!", Toast.LENGTH_LONG).show();
		        	btns.setEnabled(false);
					btne.setEnabled(true);
					edtnome.setEnabled(false);
					edtlimite.setEnabled(false);
					edtpessoa.setEnabled(false);
					B_D_D.selectconfiguracao(this);
	         }			 
		 //metodo onclick para botao btnedit
		 public void onclickedit(View v){
		Banco_de_dados db =new Banco_de_dados(this);
		db.editardados();
		    btns.setEnabled(true);
			btne.setEnabled(false);
			edtnome.setEnabled(true);
			edtlimite.setEnabled(true);
			edtpessoa.setEnabled(true);
		 }	
	
}

