package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.hidro.hidroino.R;


public class Simulador_chuveiro extends Activity{
	Chronometer ch;
	TextView gastos, textView5;
	EditText editText1;
	Button button3,button4;
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_simulador_chuveiro);
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));	    
        ch = (Chronometer)findViewById(R.id.chronometer1);
        gastos = (TextView)findViewById(R.id.gastos);
        textView5 =(TextView)findViewById(R.id.textView5);
        editText1 =(EditText)findViewById(R.id.editText1);
        button3 =(Button)findViewById(R.id.button3);
        button4 =(Button)findViewById(R.id.button4);
	    textView5.setVisibility(View.INVISIBLE);
 	    editText1.setVisibility(View.INVISIBLE); 
 	    button3.setVisibility(View.INVISIBLE);
 	    button4.setVisibility(View.INVISIBLE);
 	    Banco_de_dados2 bd2 = new Banco_de_dados2(this);
 	    bd2.selectvazaochuveiro(this);
    }
    
    public void start (View v){
        ch.setBase(SystemClock.elapsedRealtime());
        ch.start();      
    }

    public void stop(View v){       
        ch.stop();
        calculartotal();
        Banco_de_dados2 bd = new Banco_de_dados2(this);
        parametros_simulador_chuveiro pa_sc = new parametros_simulador_chuveiro();
        pa_sc.setAgua(gastos.getText().toString()); 
        bd.insertaguachuveiro(pa_sc);
    }
   public void calculartotal(){
   String temp = ch.getText().toString();
   int tm = ch.length();
   float total = 0; String hora="";
   if(tm > 5){
	
	 if(tm == 7){
	 hora = temp.substring(2,7);
	 temp = temp.substring(0,1);  
	 hora = hora.replaceAll(":", ".");
	 if(editText1.getText().toString().equals("")){
	     total = (Float.parseFloat(hora) + (Float.parseFloat(temp) * 60) )* 10;	 
	 }else{
		 total = (Float.parseFloat(hora) + (Float.parseFloat(temp) * 60) )* Float.parseFloat(editText1.getText().toString());		 
	 }
	 
	 }else{
	 hora = temp.substring(3,7);
	 temp = temp.substring(0,2);  
	 hora = hora.replaceAll(":", ".");
	 if(editText1.getText().toString().equals("")){
	     total = (Float.parseFloat(hora) + (Float.parseFloat(temp) * 60) )* 10;	 
	 }else{
		 total = (Float.parseFloat(hora) + (Float.parseFloat(temp) * 60) )* Float.parseFloat(editText1.getText().toString());		 
	 }	 
	 }
     }else{
     temp = temp.replaceAll(":", "."); 
     float  tempo = Float.parseFloat(temp);
     if(editText1.getText().toString().equals("")){
    	  total = 10 * tempo; 
	 }else{
		 total = tempo * Float.parseFloat(editText1.getText().toString());		 
	 }
   
   }
   gastos.setText(total + "L");
   }
    
    
    @Override
	public void onBackPressed() {
		Intent it = new Intent(Simulador_chuveiro.this, Simulador.class);
		startActivity(it);
		Simulador_chuveiro.this.finish();
		super.onBackPressed();
	}
    int cont = 0;
    public void configuracoes(View v){
    
      if(cont == 0){
         textView5.setVisibility(View.VISIBLE);
    	 editText1.setVisibility(View.VISIBLE); 
    	 button3.setVisibility(View.VISIBLE);
    	 button4.setVisibility(View.VISIBLE);
      cont = cont - 1;	
      }else{
    	 textView5.setVisibility(View.INVISIBLE);
     	 editText1.setVisibility(View.INVISIBLE); 
     	 button3.setVisibility(View.INVISIBLE);
     	 button4.setVisibility(View.INVISIBLE);
      cont = cont + 1;	
      }
    }
    
    public void salvar(View v){
    	parametros_simulador_chuveiro pa_sc = new parametros_simulador_chuveiro();
        pa_sc.setVazao(editText1.getText().toString());
        Banco_de_dados2 bd2 = new Banco_de_dados2(this);
        bd2.insertchuveiro(pa_sc);
        bd2.selectvazaochuveiro(this);
    }
    
    public void editar(View v){
    Banco_de_dados2 db = new Banco_de_dados2(this);
	db.editardadosvazao(); 
	button3.setEnabled(true);
	button4.setEnabled(false);
	editText1.setEnabled(true);
    }
}
