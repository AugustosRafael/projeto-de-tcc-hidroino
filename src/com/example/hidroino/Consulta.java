package com.example.hidroino;

import java.util.List;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.hidro.hidroino.R;

public class Consulta extends Activity{
	ListView listView1;
     @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_consulta);
		listView1 = (ListView) findViewById(R.id.listView1);
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));
	    
     }
     
  	@Override
  	protected void onResume() {		
  		Banco_de_dados bd = new Banco_de_dados(this);
  		List<parametros_agua> listagua = bd.selectCosumo();	
  		ArrayAdapter<parametros_agua> adp = new ArrayAdapter<parametros_agua>(this, android.R.layout.simple_list_item_1,listagua);
		listView1.setAdapter(adp);
  		super.onResume();	
  	}
	//metodo  para botao voltar
	@Override
	public void onBackPressed() {
		Intent it = new Intent(Consulta.this, Inicio.class);
		startActivity(it);
		Consulta.this.finish();
		super.onBackPressed();
	}
	
	public void onclickmodo2(View v){
	Intent it = new Intent(Consulta.this, fragmento_consulta.class );
	startActivity(it);
	Consulta.this.finish();
	}
     
}