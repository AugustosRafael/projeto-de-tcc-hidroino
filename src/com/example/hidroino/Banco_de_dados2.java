package com.example.hidroino;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Banco_de_dados2 extends SQLiteOpenHelper{
	// nome e vers�o do banco de dados
    private static final String Nome_base = "Banco_simulador_hi"; 
    private static final int Versao_base = 2; 

	//cria o banco de dados
	public Banco_de_dados2(Context context) {
		super(context, Nome_base, null, Versao_base);
	}
	
    //metodo para criar as tabelas do banco
	@Override
	public void onCreate(SQLiteDatabase db) {
	// criando e rodando tabelas
	String Tabela_vazaochuveiro = "CREATE TABLE IF NOT EXISTS vazaochuveiro("
			    + "vazao TEXT"
				+"); ";
	String Tabela_vazaomanqueira = "CREATE TABLE IF NOT EXISTS vazaomangueira("
		    + "vazao TEXT"
			+"); ";
	String Tabela_vazaovaso = "CREATE TABLE IF NOT EXISTS vazaovaso("
		    + "vazao TEXT"
			+"); ";
	String  Tabela_vazaotorneira = "CREATE TABLE IF NOT EXISTS vazaotorneira("
		    + "vazao TEXT"
			+");";	 
	String	 Tabela_agua = "CREATE TABLE IF NOT EXISTS agua("
			    + "aguachuveiro TEXT,"
			    + "aguatorneira TEXT,"
			    + "aguamaqueira TEXT,"
			    + "aguavasTabela_vazaotorneirao TEXT"
				+"); ";
	
		        db.execSQL(Tabela_vazaochuveiro); 
		        db.execSQL(Tabela_vazaomanqueira);
		        db.execSQL(Tabela_vazaovaso);
		        db.execSQL(Tabela_vazaotorneira);
		        db.execSQL(Tabela_agua); 

	}
	


	//metodo onUpgrade banco(update do banco "1 para 2 ")
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {		
		//exclui as tabelaa 
		String Tabela_vazaochuveiro = ("DROP TABLE vazaochuveiro");
		String Tabela_vazaomanqueira = ("DROP TABLE vazaomangueira");
		String Tabela_vazaovaso = ("DROP TABLE vazaovaso");
		String Tabela_vazaotorneira = ("DROP TABLE vazaotorneira");
		String Tabela_agua = ("DROP TABLE agua ");
		//rodando
		db.execSQL(Tabela_vazaochuveiro); 
        db.execSQL(Tabela_vazaomanqueira);
        db.execSQL(Tabela_vazaovaso);
        db.execSQL(Tabela_vazaotorneira);
        db.execSQL(Tabela_agua); 
		//apos excluir tenho que criar de novo a tabela
		onCreate(db);
	}
	public void insertchuveiro(parametros_simulador_chuveiro pa_sc){	
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("vazao", pa_sc.getVazao());
		db.insert("vazaochuveiro", null, cv);
		db.close();
		}
	public void insertaguachuveiro(parametros_simulador_chuveiro pa_sc){	
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("aguachuveiro", pa_sc.getVazao());
		db.insert("agua", null, cv);
		db.close();
		}
	public void selectvazaochuveiro(Simulador_chuveiro sc){
		SQLiteDatabase db= getReadableDatabase();
		String sqlSelectvazaochuveiro = "SELECT vazao FROM vazaochuveiro";		
		Cursor c = db.rawQuery(sqlSelectvazaochuveiro, null);
		String vazao; 
		if(c.moveToFirst()){		
		vazao=(c.getString(0));
		sc.button3.setEnabled(false);
		sc.button4.setEnabled(true);
		sc.editText1.setText(vazao);
		sc.editText1.setEnabled(false);
		}
		
		db.close();		
	    }
	public void editardadosvazao(){
		SQLiteDatabase db = getReadableDatabase();
		String deletetabela = ("DROP TABLE vazaochuveiro");
		db.execSQL(deletetabela);
	    onCreate(db);
	}
    
}
