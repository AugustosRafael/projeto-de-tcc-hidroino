package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.hidro.hidroino.R;

public class Tutorial extends Activity{
	  int cont = 0;
	  ImageView imageView1;
	  Button button1;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
    		setContentView(R.layout.tela_tutorial);
    		ActionBar acb = getActionBar();
    	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));   
    	    imageView1 = (ImageView)findViewById(R.id.imageView1);
    	    button1	=    (Button)findViewById(R.id.button1);
            verimagem();
           Context context = getApplicationContext();
 		   SharedPreferences sharedPref = context.getSharedPreferences("Cadastrado", Context.MODE_PRIVATE);
 		   Editor editor = sharedPref.edit();
 		   editor.putBoolean("valor", true);
 		   editor.commit();
	}
    
    public void anterior(View v){
    cont = cont - 1;
    verimagem();
    }
    
    public void proximo(View v){
	cont = cont + 1; 
    verimagem();
    }
    
   public void verimagem(){
	   if(cont == 0){ 
   	     button1.setEnabled(false); 
  		 imageView1.setImageResource(R.drawable.tutorial_um); 
		 }
	 if(cont == 1){ 
		 button1.setEnabled(true); 
		 imageView1.setImageResource(R.drawable.tutorial_dois);	 
	 }
	 if(cont == 2){
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_tres);	 
	 }
	 if(cont == 3){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_quatro);	 
	 }
	 if(cont == 4){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_cinco);	 
	 }
	 if(cont == 5){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_seis);	 
	 }
	 if(cont == 6){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_sete);	 
	 }
	 if(cont == 7){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_oito);	 
	 }
	 if(cont == 8){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_nove);	 
	 } 
	 if(cont == 9){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_dez);	 
	 }
	 if(cont == 10){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_onze);	 
	 }
	 if(cont == 11){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_doze);	 
	 }
	 if(cont == 12){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_treze);	 
	 }
	 if(cont == 13){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_quatorze);	 
	 }
	 if(cont == 14){ 
		 button1.setEnabled(true);
		 imageView1.setImageResource(R.drawable.tutorial_quinze);	 
	 }
	 if(cont == 15){  
		 Intent it = new Intent(Tutorial.this, Inicio.class);
		   startActivity(it);
		   Tutorial.this.finish();	 
	 }
   }
   
   
   }
