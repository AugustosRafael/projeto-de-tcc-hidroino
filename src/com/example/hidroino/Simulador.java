package com.example.hidroino;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.hidro.hidroino.R;


public class Simulador extends Activity{
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_simulador);
		ActionBar acb = getActionBar();
	    acb.setBackgroundDrawable(getResources().getDrawable( R.drawable.fundo_actionbar));	    
    }
    
    public void chuveiro(View v){
    	Intent it = new Intent(Simulador.this, Simulador_chuveiro.class);
		startActivity(it);
		Simulador.this.finish();	
    }
    
    @Override
	public void onBackPressed() {
		Intent it = new Intent(Simulador.this, Inicio.class);
		startActivity(it);
		Simulador.this.finish();
		super.onBackPressed();
	}
}
